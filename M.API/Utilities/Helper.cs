﻿using M.API.Components;

namespace M.API.Utilities
{
    public static class Helper
    {
        #region "Extensions"
        public static string Format(this GlobalEnum.ErrorCode errorCode)
        {
            return ((int)errorCode).ToString("D5");
        }
        #endregion
    }
}
