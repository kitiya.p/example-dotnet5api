﻿using M.API.Components;

namespace M.API.Models
{
    public class MessageToUser
    {
        public string lang_th { get; set; }
        public string lang_en { get; set; }
    }

    public class Error
    {
        public long error_id { get; set; }
        public string code { get; set; }
        public string message_to_developer { get; set; }
        public MessageToUser message_to_user { get; set; }
        public string created { get; set; }
    }

    public class APIResponseBase<T> where T : class
    {
        public GlobalEnum.HttpStatusCode http_status_code { get; set; }
        public T item { get; set; }
        public Error error { get; set; }
    }
}
