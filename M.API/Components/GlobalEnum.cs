﻿
namespace M.API.Components
{
    public class GlobalEnum
    {
        public enum HttpStatusCode
        {
            OK = 200,
            CREATED = 201,
            BAD_REQUEST = 400,
            UNAUTHORIZED = 401,
            NOTFOUND = 404,
            INTERNAL_SERVER_ERROR = 500,
            SERVICE_UNAVAILABLE = 503
        }

        public enum ErrorCode
        {
            NOTFOUND = 404
        }
    }
}
