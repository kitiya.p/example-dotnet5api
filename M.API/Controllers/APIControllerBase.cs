﻿using M.API.Components;
using M.API.Configurations;
using M.API.Models;
using M.API.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace M.API.Controllers
{
    [ApiController]
    public class APIControllerBase<T> : ControllerBase where T : class
    {
        #region "Generate response of api layer"
        public ActionResult ReturnActionResult<R>(GlobalEnum.HttpStatusCode httpStatusCode
            , R response = null, Error error = null) where R : class
        {
            var _responseBase = new APIResponseBase<R>();
            _responseBase.http_status_code = httpStatusCode;
            _responseBase.item = response;
            _responseBase.error = error;

            switch (_responseBase.http_status_code)
            {
                case GlobalEnum.HttpStatusCode.OK:
                    {
                        if (_responseBase.item != null) return Ok(_responseBase.item);
                        else return NotFound(new Error()
                        {
                            error_id = -404,
                            code = GlobalEnum.ErrorCode.NOTFOUND.Format(),
                            message_to_developer = "",
                            message_to_user = new MessageToUser()
                            {
                                lang_th = GlobalSetting.MSG_NOTFOUND_TH,
                                lang_en = GlobalSetting.MSG_NOTFOUND_EN
                            },
                            created = DateTime.Now.ToString(GlobalSetting.DATETIME_FORMAT)
                        }); ; ;
                    }
                case GlobalEnum.HttpStatusCode.CREATED: return StatusCode((int)HttpStatusCode.Created, _responseBase.item);
                case GlobalEnum.HttpStatusCode.NOTFOUND: return NotFound(new { _responseBase.error });
                case GlobalEnum.HttpStatusCode.INTERNAL_SERVER_ERROR: return StatusCode((int)HttpStatusCode.InternalServerError, new { _responseBase.error });
                default: return StatusCode((int)_responseBase.http_status_code, _responseBase);
            }
        }
        #endregion
    }
}
