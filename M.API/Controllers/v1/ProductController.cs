﻿using M.API.Components;
using M.BLL.Models.v1;
using M.BLL.Services.v1;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace M.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ProductController : APIControllerBase<ProductController>
    {
        private readonly IProductService mService;

        public ProductController(IProductService productService)
        {
            mService = productService;
        }

        [HttpPost("search")]
        public async Task<IActionResult> SearchProduct(SearchProductRequest request)
        {
            var _response = await mService.SearchProduct(request);
            return ReturnActionResult<SearchProductResponse>(GlobalEnum.HttpStatusCode.OK
                , _response);
        }
    }
}
