﻿using M.API.Components;
using M.BLL.Models.v1;
using M.BLL.Services.v1;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace M.API.Controllers.v1
{

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/product-type")]
    public class ProductTypeController : APIControllerBase<ProductTypeController>
    {
        private readonly IProductTypeService mService;

        public ProductTypeController(IProductTypeService ProductTypeService)
        {
            mService = ProductTypeService;
        }

        [HttpPost("search")]
        public async Task<IActionResult> SearchProductType(SearchProductTypeRequest request)
        {
            var _response = await mService.SearchProductType(request);
            return ReturnActionResult<SearchProductTypeResponse>(GlobalEnum.HttpStatusCode.OK
                , _response);
        }
    }
}
