﻿
namespace M.API.Configurations
{
    public class GlobalSetting
    {
        public const string DATETIME_FORMAT = "yyyy-MM-ddTHH:mm:ss.fff zzz";

        public const string MSG_NOTFOUND_TH = "ไม่พบสิ่งที่คุณค้นหา";
        public const string MSG_NOTFOUND_EN = "No results found.";
    }
}
