﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace M.API.Filters
{
    public class APIAsyncActionFilter : IAsyncActionFilter, IAsyncResultFilter
    {
        public APIAsyncActionFilter()
        {
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // Do something before the action executes.

            // next() calls the action method.
            var _resultContext = await next();
            // resultContext.Result is set.
            // Do something after the action executes.
        }

        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
        {
            if (!(context.Result is EmptyResult))
            {
                await next();
            }
            else
            {
                context.Cancel = true;
            }
        }
    }
}
