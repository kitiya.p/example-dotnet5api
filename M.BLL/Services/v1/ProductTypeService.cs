﻿using M.BLL.Models.v1;
using M.BLL.Repositories;
using M.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M.BLL.Services.v1
{
    public interface IProductTypeService
    {
        Task<SearchProductTypeResponse> SearchProductType(SearchProductTypeRequest request);
    }

    public class ProductTypeService : ServiceBase<ProductTypeService>, IProductTypeService
    {
        public ProductTypeService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public async Task<SearchProductTypeResponse> SearchProductType(SearchProductTypeRequest request)
        {
            var _productTypeList = new List<ProductType>();
            if (request.id > 0)
            {
                var _productType = await mUnitOfWork.ProductTypes.GetById(request.id);
                if (_productType != null) _productTypeList.Add(_productType);
            }
            else if (!string.IsNullOrEmpty(request.name))
            {
                var _filterList = await mUnitOfWork.ProductTypes.Find(x => x.Name.Contains(request.name));
                if (_filterList.Count() > 0) _productTypeList.AddRange(_filterList);
            }
            else 
            {
                var _filterList = await mUnitOfWork.ProductTypes.All();
                if (_filterList != null) _productTypeList.AddRange(_filterList);
            }

            if (_productTypeList.Count() > 0)
            {
                var _response = new SearchProductTypeResponse();
                _response.product_types = _productTypeList.Select(x => new SearchProductType()
                    {
                        id = x.ID,
                        name = x.Name
                    }).ToList();

                return _response;
            }

            return null;
        }
    }
}
