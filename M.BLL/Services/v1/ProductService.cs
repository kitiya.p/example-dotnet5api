﻿using M.BLL.Models.v1;
using M.BLL.Repositories;
using M.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M.BLL.Services.v1
{
    public interface IProductService
    {
        Task<SearchProductResponse> SearchProduct(SearchProductRequest request);
    }

    public class ProductService : ServiceBase<ProductService>, IProductService
    {
        public ProductService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        public async Task<SearchProductResponse> SearchProduct(SearchProductRequest request)
        {
            var _productList = new List<Product>();
            if (request.id > 0)
            {
                var _product = await mUnitOfWork.Products.GetByIdWithPT(request.id);
                if (_product != null) _productList.Add(_product);
            }
            else if (!string.IsNullOrEmpty(request.name))
            {
                if (request.product_type_id > 0)
                {
                    var _filterList = await mUnitOfWork.Products.FindWithPT(x => x.Name.Contains(request.name)
                        && x.ProductTypeID == request.product_type_id);
                    if (_filterList.Count() > 0) _productList.AddRange(_filterList);
                }
                else if (!string.IsNullOrEmpty(request.product_type_name))
                {
                    var _filterList = (await mUnitOfWork.Products.FindWithPT(x => x.Name.Contains(request.name)))
                        .Where(x => x.ProductType != null && x.ProductType.Name.Contains(request.product_type_name));
                    if (_filterList.Count() > 0)
                    {
                        _productList.AddRange(_filterList);
                    }
                }
                else
                {
                    var _filterList = await mUnitOfWork.Products.FindWithPT(x => x.Name.Contains(request.name));
                    if (_filterList.Count() > 0) _productList.AddRange(_filterList);
                }
            }
            else if (request.product_type_id > 0)
            {
                var _filterList = await mUnitOfWork.Products.FindWithPT(x => x.ProductTypeID == request.product_type_id);
                if (_filterList.Count() > 0) _productList.AddRange(_filterList);
            }
            else if (!string.IsNullOrEmpty(request.product_type_name))
            {
                var _filterList = await mUnitOfWork.ProductTypes.FindWithP(x => x.Name.Contains(request.product_type_name));
                if (_filterList.Count() > 0) _productList.AddRange(_filterList);
            }
            else
            {
                var _filterList = await mUnitOfWork.Products.AllWithPT();
                if (_filterList.Count() > 0) _productList.AddRange(_filterList);
            }

            if (_productList.Count() > 0)
            {
                var _response = new SearchProductResponse();
                _response.product_types = _productList.GroupBy(g => g.ProductType)
                    .Select(s => new SearchProductWithPT
                    {
                        product_type_id = s.Key.ID,
                        produt_type_name = s.Key.Name,
                        products = s.Select(p => new SearchProduct()
                        {
                            id = p.ID,
                            name = p.Name
                        }).ToList()
                    }).ToList();

                return _response;
            }

            return null;
        }
    }
}
