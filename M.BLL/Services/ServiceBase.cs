﻿
using M.BLL.Repositories;

namespace M.BLL.Services
{
    public class ServiceBase<T> where T : class
    {
        protected readonly IUnitOfWork mUnitOfWork;

        public ServiceBase(IUnitOfWork unitOfWork)
        {
            this.mUnitOfWork = unitOfWork;
        }
    }
}
