﻿using M.DAL.Database;
using M.DAL.Repositories;
using System;
using System.Threading.Tasks;

namespace M.BLL.Repositories
{
    public interface IUnitOfWork
    {
        IProductTypeRepository ProductTypes { get; }
        IProductRepository Products { get; }

        Task CompleteAsync();
    }

    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ProductContext mContext;

        private ProductTypeRepository mProductTypes;
        private ProductRepository mProducts;

        public IProductTypeRepository ProductTypes => mProductTypes ??= new ProductTypeRepository(mContext);
        public IProductRepository Products => mProducts ??= new ProductRepository(mContext);

        public UnitOfWork(ProductContext context)
        {
            mContext = context;
        }

        public async Task CompleteAsync()
        {
            await mContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            mContext.Dispose();
        }
    }
}
