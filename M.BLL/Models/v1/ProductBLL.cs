﻿using System.Collections.Generic;

namespace M.BLL.Models.v1
{
    #region "SearchProduct"
    public class SearchProductRequest
    {
        public long id { get; set; }
        public string name { get; set; }
        public long product_type_id{ get; set; }
        public string product_type_name { get; set; }
    }

    public class SearchProduct
    {
        public long id { get; set; }
        public string name { get; set; }
    }

    public class SearchProductWithPT
    {
        public long product_type_id { get; set; }
        public string produt_type_name { get; set; }
        public List<SearchProduct> products { get; set; }
    }

    public class SearchProductResponse
    {
        public List<SearchProductWithPT> product_types { get; set; }
    }
    #endregion
}
