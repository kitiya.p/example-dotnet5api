﻿using System.Collections.Generic;

namespace M.BLL.Models.v1
{
    #region "SearchProductType"
    public class SearchProductTypeRequest
    {
        public long id { get; set; }
        public string name { get; set; }
    }
    public class SearchProductType
    {
        public long id { get; set; }
        public string name { get; set; }
    }

    public class SearchProductTypeResponse
    {
        public List<SearchProductType> product_types { get; set; }
    }
    #endregion
}
