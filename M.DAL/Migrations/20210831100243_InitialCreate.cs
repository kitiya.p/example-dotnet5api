﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace M.DAL.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false, comment: "Product id")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductTypeID = table.Column<long>(type: "bigint", nullable: false, comment: "Product type id"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false, comment: "Product name"),
                    Description = table.Column<string>(type: "nvarchar(1000)", nullable: true, comment: "Product description"),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "Record create date"),
                    CreateBy = table.Column<string>(type: "nvarchar(50)", nullable: false, comment: "Record create by"),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "Record last update date"),
                    UpdateBy = table.Column<string>(type: "nvarchar(50)", nullable: true, comment: "Record last update by")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ProductType",
                columns: table => new
                {
                    ID = table.Column<long>(type: "bigint", nullable: false, comment: "Product type id")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false, comment: "Product type name"),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "Record create date"),
                    CreateBy = table.Column<string>(type: "nvarchar(50)", nullable: false, comment: "Record create by"),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true, comment: "Record last update date"),
                    UpdateBy = table.Column<string>(type: "nvarchar(50)", nullable: true, comment: "Record last update by")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductType", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductType");
        }
    }
}
