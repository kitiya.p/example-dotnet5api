﻿using M.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace M.DAL.Database
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
           : base(options)
        {
        }

        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// 1. install Microsoft.EntityFrameworkCore.SqlServer
        /// 2. Microsoft.EntityFrameworkCore.Tools
        /// 3. open Package MangerConsole
        /// 4. select "Default project" to project include "DbContext", ex "DAL"
        /// 5. run cmd "Add-Migration InitialCreate" for create script
        /// 6. run cmd "Update-Database" for commit
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
