﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace M.DAL.Models
{
    [Table("ProductType")]
    public class ProductType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Comment("Product type id")]
        public long ID { get; set; }
        [Required, Column(TypeName = "nvarchar(100)"), Comment("Product type name")]
        public string Name { get; set; }
        [Required, Comment("Record create date")]
        public DateTime CreateDate { get; set; }
        [Required, Column(TypeName = "nvarchar(50)"), Comment("Record create by")]
        public string CreateBy { get; set; }
        [Comment("Record last update date")]
        public DateTime? UpdateDate { get; set; }
        [Column(TypeName = "nvarchar(50)"), Comment("Record last update by")]
        public string UpdateBy { get; set; }

        [NotMapped]
        public ICollection<Product> Products { get; set; }
    }
}
