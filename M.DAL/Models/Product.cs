﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace M.DAL.Models
{
    [Table("Product")]
    public class Product
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Comment("Product id")]
        public long ID { get; set; }
        [Required, Comment("Product type id")]
        public long ProductTypeID { get; set; }
        [Required, Column(TypeName = "nvarchar(100)"), Comment("Product name")]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(1000)"), Comment("Product description")]
        public string Description { get; set; }
        [Required, Comment("Record create date")]
        public DateTime CreateDate { get; set; }
        [Required, Column(TypeName = "nvarchar(50)"), Comment("Record create by")]
        public string CreateBy { get; set; }
        [Comment("Record last update date")]
        public DateTime? UpdateDate { get; set; }
        [Column(TypeName = "nvarchar(50)"), Comment("Record last update by")]
        public string UpdateBy { get; set; }

        [NotMapped]
        public ProductType ProductType { get; set; }
    }
}
