﻿using M.DAL.Database;
using M.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace M.DAL.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        Task<IEnumerable<Product>> AllWithPT();
        Task<Product> GetByIdWithPT(long id);
        Task<IEnumerable<Product>> FindWithPT(Expression<Func<Product, bool>> predicate);
    }

    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(ProductContext context)
            : base(context)
        {
        }

        private ProductContext mProductContext
        {
            get { return mContext as ProductContext; }
        }

        public async Task<IEnumerable<Product>> AllWithPT()
        {
            var _list = await All();
            if (_list.Count() > 0)
            {
                _list = _list.Select(async i =>
                {
                    i.ProductType = await mProductContext.ProductTypes.FindAsync(i.ProductTypeID);
                    return i;
                }).Select(t => t.Result);
            }

            return _list;
        }

        public async Task<Product> GetByIdWithPT(long id)
        {
            var _item = await GetById(id);
            if (_item != null)
                _item.ProductType = await mProductContext.ProductTypes.FindAsync(_item.ProductTypeID);

            return _item;
        }

        public async Task<IEnumerable<Product>> FindWithPT(Expression<Func<Product, bool>> predicate)
        {
            var _list = await Find(predicate);
            if (_list.Count() > 0)
            {
                _list = _list.Select(async i =>
                {
                    i.ProductType = await mProductContext.ProductTypes.FindAsync(i.ProductTypeID);
                    return i;
                }).Select(t => t.Result);
            }

            return _list;
        }
    }
}
