﻿using M.DAL.Database;
using M.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace M.DAL.Repositories
{
    public interface IProductTypeRepository : IGenericRepository<ProductType>
    {
        Task<IEnumerable<Product>> FindWithP(Expression<Func<ProductType, bool>> predicate);
    }

    public class ProductTypeRepository : GenericRepository<ProductType>, IProductTypeRepository
    {
        public ProductTypeRepository(ProductContext context) : base(context) 
        {
        }

        private ProductContext mProductContext
        {
            get { return mContext as ProductContext; }
        }

        public async Task<IEnumerable<Product>> FindWithP(Expression<Func<ProductType, bool>> predicate)
        {
            var _filter = await Find(predicate);
            if (_filter.Count() > 0)
            {
                _filter = _filter.Select(async i =>
                {
                    i.Products = await mProductContext.Products.Where(x => x.ProductTypeID == i.ID).ToListAsync();
                    return i;
                }).Select(t => t.Result);
            }

            var _list = _filter.SelectMany(x => x.Products, (parent, child) => { parent.Products = null;
                child.ProductType = parent; return child; });

            return _list;
        }
    }
}
