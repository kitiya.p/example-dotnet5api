﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace M.DAL.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> All();
        Task<T> GetById(long id);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task<bool> Add(T entity);
        Task<bool> Update(T entity);
        Task<bool> Delete(long id);
    }

    public class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        protected readonly DbContext mContext;
        protected DbSet<T> mData;

        public GenericRepository(DbContext context)
        {
            this.mContext = context;
            this.mData = context.Set<T>();
        }

        public virtual async Task<IEnumerable<T>> All()
        {
            return await mData.ToListAsync();
        }

        public virtual async Task<T> GetById(long id)
        {
            return await mData.FindAsync(id);
        }

        public virtual async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await mData.Where(predicate).ToListAsync();
        }

        public virtual async Task<bool> Add(T entity)
        {
            await mData.AddAsync(entity);
            return true;
        }

        public virtual async Task<bool> Update(T entity)
        {
            mData.Update(entity);
            await mContext.SaveChangesAsync();
            return true;
        }

        public virtual async Task<bool> Delete(long id)
        {
            var _entity = new T();
            if (_entity.GetType().GetProperties().Any(x => x.Name == "ID")
                && _entity.GetType().GetProperty("ID") is PropertyInfo pi
                && pi.PropertyType == typeof(long))
            {
                pi.SetValue(null, id);

                mData.Remove(_entity);
                await mContext.SaveChangesAsync();

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
