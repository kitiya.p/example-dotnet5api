
  INSERT INTO [ProductType]([Name], [CreateDate], [CreateBy]) VALUES('Milk', GETDATE(), 'initial')
  INSERT INTO [ProductType]([Name], [CreateDate], [CreateBy]) VALUES('Snack', GETDATE(), 'initial')
  INSERT INTO [ProductType]([Name], [CreateDate], [CreateBy]) VALUES('Bread', GETDATE(), 'initial')
  INSERT INTO [ProductType]([Name], [CreateDate], [CreateBy]) VALUES('Meat', GETDATE(), 'initial')
  INSERT INTO [ProductType]([Name], [CreateDate], [CreateBy]) VALUES('Soft Drink', GETDATE(), 'initial')

  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(1, 'Meiji Strawberry Milk', 'Fresh Cow''s Milk.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(1, 'Meiji Chocolate Milk', 'Dark Chocolate 70%.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(1, 'Meiji Milk', 'Meiji Pasteurized 100%.', GETDATE(), 'initial')

  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(2, 'Lays Potato Chips (Classic)', 'Best quality potatoes.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(2, 'TARO Fish', 'Great Taste with Nutritional Benefits.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(2, 'Pringles Potato Chips', 'This is a Vegetarian product.', GETDATE(), 'initial')

  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(3, 'Sliced Butter Bread', 'Delicious.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(3, 'Crustless Bread', 'Baked goods.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(3, 'Pineapple Pie', 'Freshly baking.', GETDATE(), 'initial')

  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(4, 'CP Pork Sausage', 'Pork.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(4, 'CP Kurobuta', 'Premium.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(4, 'PFP Crab Stick Fried', 'Fish Frozen.', GETDATE(), 'initial')

  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(5, 'Cola', 'Coca-Cola.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(5, 'Fanta Orange', 'Fanta.', GETDATE(), 'initial')
  INSERT INTO [Product]([ProductTypeID], [Name], [Description], [CreateDate],[CreateBy]) VALUES(5, 'Singha Soda', 'Singha.', GETDATE(), 'initial')